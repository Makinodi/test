//
// Created by Ma Park on 10/01/2021.
//
#ifndef AMP_NTP_LIST_H
#define AMP_NTP_LIST_H

#include <arpa/inet.h>

typedef struct {
	in_addr_t *values;
	size_t    len;
} list_t;


list_t *make_list(const char *filename);

#endif //AMP_NTP_LIST_H
