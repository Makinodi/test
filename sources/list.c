//
// Created by Ma Park on 10/01/2021.
//

#include <string.h>
#include <errno.h>
#include <malloc.h>
#include <arpa/inet.h>
#include "list.h"

list_t *make_list(const char *filename)
{
	list_t *list = malloc(sizeof(list_t));
	char   *line = NULL;
	FILE   *file = NULL;
	size_t len   = 0;

	if ((file = fopen(filename, "r")) == NULL) {
		dprintf(2, "open: %s\n", strerror(errno));
		return NULL;
	}
	while ((getline(&line, &len, file)) != -1) {
		list->values = realloc(list->values, sizeof(in_addr_t) * (list->len + 1));
		list->values[list->len] = inet_addr(line);
		list->len++;
	}
	return list;
}