//
// Created by Ma Park on 10/01/2021.
//

#include <stdint.h>
#include <stddef.h>

uint16_t chksum(const char buf[], size_t buflen)
{
	uint32_t sum = 0, i;

	if (buflen < 1)
		return 0;
	for (i = 0; i < buflen - 1; i += 2)
		sum += *(char *) &buf[i];
	if (buflen & 1)
		sum += buf[buflen - 1];
	return ~((sum >> 16) + (sum & 0xffff));
}